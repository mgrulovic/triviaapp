import { Component, OnInit } from '@angular/core';

import { TriviaService } from './trivia.service';
 
@Component({
  selector: 'app-trivia-app',
  templateUrl: './trivia-app.component.html',
  styleUrls: ['./trivia-app.component.scss']
})
export class TriviaAppComponent implements OnInit {
  private trivia: Array<{}>;
  private categories: Array<{}>;
  constructor(private service: TriviaService) { }

  async ngOnInit() {
    this.trivia = await this.service.getRandomTrivia();
    this.categories = await this.service.getCategories();
  }

  async getTriviaByCategory(category) {
    this.trivia = await this.service.getTriviaByCategory(category);
    console.log(this.trivia);
  }

}
