import { Injectable } from '@angular/core';
import axios, { AxiosInstance } from "axios";

@Injectable({
  providedIn: 'root'
})
export class TriviaService {
  private axiosClient: {};
  constructor() { 
    this.axiosClient = axios.create({
      timeout: 3000,
      headers: {
      }
  });
  }

  async getRandomTrivia() {
    try {
      const trivia =  await axios.get('http://jservice.io/api/random', {
        params: {
          count: 30
        }
      });
      return trivia.data;
    } catch (error) {
      console.log(error);
    }
  }
  async getCategories() {
    try {
      const categories =  await axios.get('http://jservice.io/api/categories', {
        params: {
          count: 10
        }
      });
      return categories.data;
    } catch (error) {
      console.log(error);
    }
  }
  async getTriviaByCategory(category) {
    try {
      const trivia =  await axios.get('http://jservice.io/api/clues', {
        params: {
          category,
        }
      });
      return trivia.data;
    } catch (error) {
      console.log(error);
    }
  }
}
