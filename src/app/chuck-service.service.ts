import { Injectable } from '@angular/core';
import Chuck from 'chucknorris-io';

@Injectable({
  providedIn: 'root'
})
export class ChuckServiceService {
  private _chuckClient: any;

  constructor() {
    this._chuckClient = new Chuck();
  }
  async getRandomJoke(category: string = '') {
    try {
      const response = await this._chuckClient.getRandomJoke(category);
      return response;
    } catch (error) {
      throw error;
    }
  }
}
