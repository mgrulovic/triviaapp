import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { ChuckServiceService } from '../chuck-service.service';

@Injectable({
  providedIn: 'root'
})
export class RandomJokeResolver implements Resolve<any> {
  private chuckService: ChuckServiceService = new ChuckServiceService();
  constructor() {
  }
  async resolve(route: ActivatedRouteSnapshot, rstate: RouterStateSnapshot) {
    try {
      const response = await this.chuckService.getRandomJoke();
      return response;
    } catch (error) {
      throw error;
    }
  }
}
