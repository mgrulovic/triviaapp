import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChuckNorrisComponentComponent } from './chuck-norris.component';

describe('ChuckNorrisComponentComponent', () => {
  let component: ChuckNorrisComponentComponent;
  let fixture: ComponentFixture<ChuckNorrisComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChuckNorrisComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChuckNorrisComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
