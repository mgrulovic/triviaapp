import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { ChuckServiceService } from '../chuck-service.service';

@Component({
  selector: 'app-chuck-norris-component',
  templateUrl: './chuck-norris.component.html',
  styleUrls: ['./chuck-norris.component.scss']
})
export class ChuckNorrisComponentComponent implements OnInit {
  private joke: {} = {};
  jokeCategoryForm = new FormGroup ({
    category: new FormControl(),
  });
  constructor(private actr: ActivatedRoute, private chuckService: ChuckServiceService) {}

  async ngOnInit() {
    this.actr.data.subscribe((data) => {
      this.joke = data.joke;
    });
  }

  async getJokeByCategory() {
    this.joke = await this.chuckService.getRandomJoke(this.jokeCategoryForm.get('category').value);
  }
}
