import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TriviaAppComponent } from './trivia-app/trivia-app.component';
import { ChuckNorrisComponentComponent } from './chuck-norris/chuck-norris.component';
import { RandomJokeResolver } from './chuck-norris/random-joke-resolver';

const appRoutes: Routes = [
  { path: '', component: TriviaAppComponent },
  {
    path: 'chuck',
    component: ChuckNorrisComponentComponent,
    resolve: {
      joke: RandomJokeResolver
    }
  },
  { path: '**', component: TriviaAppComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    TriviaAppComponent,
    ChuckNorrisComponentComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [RandomJokeResolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
